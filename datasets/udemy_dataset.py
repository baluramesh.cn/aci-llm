from .base_dataset import BaseDataset
import json
import os

class UdemyDataset(BaseDataset):
    _instance = None
    filepath = 'data_processing/datasets/udemy.jsonl'

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls._instance = super(UdemyDataset, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def load(self):
        # Check if the file exists
        if not os.path.exists(self.filepath):
            raise ValueError(f"File {self.filepath} does not exist.")

        # Loading data from json file
        with open(self.filepath, "r") as f:
            self.data = json.load(f)