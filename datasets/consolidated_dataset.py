from .base_dataset import BaseDataset
import json
import os

class ConsolidatedDataset(BaseDataset):
    _instance = None
    filepath = 'data_processing/datasets/consolidated_questions.jsonl'
    filt_answer = True

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls._instance = super(ConsolidatedDataset, cls).__new__(cls, *args, **kwargs)
        return cls._instance
    
    def load(self):
        # Check if file exists
        if not os.path.exists(self.filepath):
            raise ValueError(f"File {self.filepath} does not exist.")
        
        # loads data from the json file
        with open(self.filepath, "r") as file:
            self.data = json.load(file)

        # filters for only one answer and not multiple
        if self.filt_answer:
            self.data = [q for q in self.data if isinstance(q["answer"], str)]
        