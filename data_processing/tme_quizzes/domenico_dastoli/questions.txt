Which of the following controllers (if any) uses a mostly centralized control plane model?
a: OpenDaylight Controller
b: Cisco Application Policy Infrastructure Controller (APIC)
c: Cisco APIC Enterprise Module (APIC-EM)
d: None of these controllers uses a mostly centralized control plane.

Which modes of operation are available in Nexus 9000 series switches?
a: NX-OS and ACI
b: IOS and Junos
c: CLI and GUI
d: Layer 2 and Layer 3

Which type of connectivity is not allowed in ACI mode of operation?
a: Leaf to Leaf
b: Spine to Spine
c: Leaf to Leaf and Spine to Spine
d: All of the above

What is the role of the APIC controller in an ACI fabric?
a: It serves as the central configuration point of the ACI solution.
b: It connects the endpoints to the fabric switches.
c: It manages the routing protocols in the ACI fabric.
d: It provides Layer 2 switching functionality in the fabric.

In an ACI Spine-Leaf architecture, where are servers typically connected?
a: They are connected to Spine switches.
b: They are connected to Leaf switches.
c: They are connected to both Spine and Leaf switches.
d: They are not directly connected to switches in this architecture.

What is the main difference between a VLAN and a Bridge Domain in Cisco ACI?
a: VLAN can carry multiple subnets, while a Bridge Domain represents a single network.
b: VLAN is a layer 2 construct, while a Bridge Domain is a layer 3 construct.
c: Bridge Domain uses VXLAN Network Identifier (VNI), while VLAN uses traditional VLAN IDs.
d: VLAN is used for inter-VRF communication, while a Bridge Domain is used for intra-VRF communication.

Which Cisco 9K models are commonly used as Spine Nodes in an ACI setup?
a: 9316D-GX
b: 9332D-GX2B
c: 9336PQ
d: 9364D-GX2A
e: 9364C
f: All of the above

Can we integrate the management of third-party devices in the APIC controller?
a: Yes, the APIC controller allows integration and management of third-party devices.
b: No, the APIC controller does not support the management of third-party devices.

What is the role of VXLAN in the ACI fabric?
a: VXLAN provides layer 3 routing between different ACI fabrics.
b: VXLAN enables the use of Layer 2 VLANs for network segmentation.
c: VXLAN extends Layer 2 segments over Layer 3 infrastructure in the ACI fabric.
d: VXLAN facilitates the integration of third-party devices into the ACI fabric.

Which Cisco APIC CLI command can you use to verify the active/standby links on the Cisco APIC bond interface connected to the Cisco ACI fabric?
a: cat /proc/net/bonding/bond0
b: cat /proc/net/bonding/bond1
c: verify interface bond0
d: verify interface bond1

You are troubleshooting the logical construct configuration in a Cisco ACI fabric, and you want to ping an endpoint with IP address 10.1.1.1, from a leaf switch CLI using the pervasive gateway IP address 10.1.1.254, as a source IP address. Which command should you use for the tenant Sales and VRF Presales_VRF?
a: ping -T Sales - V Presales_VRF -S 10.1.1.254 10.1.1.1
b: iping -T Sales - V Presales_VRF -S 10.1.1.254 10.1.1.1
c: ping -V Sales:Presales_VRF -S 10.1.1.254 10.1.1.1
d: iping -V Sales:Presales_VRF -S 10.1.1.254 10.1.1.1

Which three are logical constructs in the Cisco ACI policy model that are part of the tenant hierarchy? (Choose three.)
a: Bridge domain
b: VLAN pools
c: VRF
d: EPG
e: AAEP
f: Domain

Which statement is correct regarding Cisco ACI Integration with Cisco AVE?
a: Cisco AVE is hypervisor dependent.
b: Cisco AVE resides in the hypervisor kernel.
c: Cisco AVE cannot be integrated within the VMware virtual infrastructure.
d: Cisco AVE implements the OpFlex protocol for control plane communication.

Which option should you use to create a Port Channel interface in Cisco ACI?
a: switch policy
b: switch profile
c: interface policy group
d: interface profile

Which ASIC is used on second-generation Cisco Nexus 9300 Series switches?
a: NFE
b: ALE
c: LSE
d: UPC

Which two protocols are used in the Cisco ACI fabric discovery process? (Choose two.)
a: Cisco Discovery Protocol
b: LLDP
c: DHCP
d: LACP
e: SNMP

Which two statements regarding Cisco APIC cluster are correct? (Choose two.)
a: The Cisco APIC cluster sits in the traffic path.
b: The Cisco APIC cluster uses a large database technology called sharding.
c: A cluster size of 4 or 6 APICs is recommended.
d: The shards are distributed across two Cisco APICs for redundancy.
e: Any controller in the Cisco APIC cluster can service any user for any operation.

You plan to use a service graph template to insert a service appliance in the traffic path in Cisco ACI. With which element should you associate the service graph template to be 'rendered' on the Layer 4 to Layer 7 device and on the fabric?
a: tenant
b: bridge domain
c: VRF
d: contract

Which CLI command on the spine and leaf switches in the Cisco ACI should you use to display the switches in-band management IP addresses?
a: show ip interf vrf management
b: show inband-mgmt
c: show ip interf inband-mgmt
d: show ip interf inband-mgmt vrf mgmt:inb

You are performing an upgrade on Cisco APIC from Release 2.2(4) to Release 4.2(1). Which statement is correct?
a: Only the switches will be upgraded.
b: All controllers in the Cisco APIC cluster are upgraded at the same time.
c: The system automatically checks the compatibility for possible upgrades by default.
d: You can upgrade only the Cisco APIC, without the need to upgrade the leaf and spine switches later.

