Which of the following controllers (if any) uses a mostly centralized control plane model?
a: OpenDaylight Controller
b: Cisco Application Policy Infrastructure Controller (APIC)
c: Cisco APIC Enterprise Module (APIC-EM)
d: None of these controllers uses a mostly centralized control plane.

How many APIC controllers are recommended in an ACI deployment of 70 switches?
a: Five
b: Two
c: Three
d: Four

How many APIC controllers are recommended in an ACI deployment of 250 switches?
a: Seven
b: Two
c: Three
d: Five

What is the role of the APIC controller in an ACI fabric?
a: It serves as the central configuration point of the ACI solution.
b: It connects the endpoints to the fabric switches.
c: It manages the routing protocols in the ACI fabric.
d: It provides Layer 2 switching functionality in the fabric.

What is an End Point Group (EPG) in Cisco ACI?
a: A logical grouping of endpoints with similar characteristics
b: A physical device connected directly to the network
c: A virtual machine used for network testing
d: An IP address assigned to an endpoint

Can one EPG (Endpoint Group) be linked to multiple Bridge Domains in Cisco ACI?
a: Yes, an EPG can be referenced to multiple Bridge Domains.
b: No, a single EPG cannot be linked to multiple Bridge Domains.
c: It depends on the configuration of the Bridge Domains and EPG contracts.
d: Multiple EPGs can be linked to multiple Bridge Domains.

What is meant by Application Profile in Cisco ACI?
a: Application Profiles are containers that group together endpoint groups (EPGs) in Cisco ACI.
b: Application Profiles define the network policies for external connectivity in ACI.
c: Application Profiles are used to manage the physical infrastructure of the ACI fabric.
d: Application Profiles are created within the bridge domains to organize network resources.

What is meant by Private Network or VRF (Virtual Routing and Forwarding) in Cisco ACI?
a: Private Network or VRF refers to a separate virtual network within ACI that contains L3 routing instances and IP addresses.
b: Private Network or VRF is a network segment isolated from the public internet for enhanced security.
c: Private Network or VRF is a specialized network for virtual machines and cloud-based applications.
d: Private Network or VRF is a term used to describe a network with restricted access and limited connectivity.

Which Cisco 9K models are commonly used as Spine Nodes in an ACI setup?
a: 9316D-GX
b: 9332D-GX2B
c: 9336PQ
d: 9364D-GX2A
e: 9364C
f: All of the above

Which Cisco 9K models are commonly used as Leaf Nodes in an ACI setup?
a: C9316W-YMCA
b: C93600CD-GX
c: 9364C
d: All of the above

Can we connect Access Layer switches in the downlink to Leaf Nodes in an ACI setup?
a: Yes, we can connect Access Layer switches to Leaf Nodes in an ACI setup.
b: No, Access Layer switches cannot be connected to Leaf Nodes in an ACI setup.

You are troubleshooting the logical construct configuration in a Cisco ACI fabric, and you want to ping an endpoint with IP address 10.1.1.1, from a leaf switch CLI using the pervasive gateway IP address 10.1.1.254, as a source IP address. Which command should you use for the tenant Sales and VRF Presales_VRF?
a: ping -T Sales - V Presales_VRF -S 10.1.1.254 10.1.1.1
b: iping -T Sales - V Presales_VRF -S 10.1.1.254 10.1.1.1
c: ping -V Sales:Presales_VRF -S 10.1.1.254 10.1.1.1
d: iping -V Sales:Presales_VRF -S 10.1.1.254 10.1.1.1

Which construct marries the physical world (Fabric tab in the Cisco APIC GUI) to the logical world (Tenant tab in the Cisco APIC GUI)?
a: Tenant
b: VLAN pools
c: VRF
d: AAEP
e: Domain

Which statement is correct regarding Cisco ACI Layer 4–7 service insertion modes?
a: The use of configurations with device package (managed mode) is the recommended mode.
b: With PBR, the Layer 4–7 device needs to be the default gateway for endpoints connected to the Cisco ACI fabric.
c: The Layer 4–7 service insertion feature enables you to insert more than one service between two EPGs, and create a service chain between them.
d: You cannot insert Layer 4–7 devices in an unmanaged mode and use the service graph with PBR.

Which two statements regarding Cisco APIC cluster are correct? (Choose two.)
a: The Cisco APIC cluster sits in the traffic path.
b: The Cisco APIC cluster uses a large database technology called sharding.
c: A cluster size of 4 or 6 APICs is recommended.
d: The shards are distributed across two Cisco APICs for redundancy.
e: Any controller in the Cisco APIC cluster can service any user for any operation.

Which CLI command on the spine and leaf switches in the Cisco ACI should you use to display the switches in-band management IP addresses?
a: show ip interf vrf management
b: show inband-mgmt
c: show ip interf inband-mgmt
d: show ip interf inband-mgmt vrf mgmt:inb

Which statement is correct regarding endpoint learning process in Cisco ACI?
a: The leaf switches store location and policy information only about endpoints that are directly attached to them.
b: The leaf switches store location and policy information only about endpoints that are directly attached to them or through a directly attached Layer 2 switch or Fabric Extender.
c: The leaf switches store location and policy information about endpoints that are attached directly to them (or through a directly attached Layer 2 switch or Fabric Extender), and endpoints that are attached to other leaf switches on the fabric.
d: The leaf switches do not store location and policy information about any endpoints, since it is stored in the Cisco APIC.

You are creating an application profile in Cisco ACI to group physical and virtual servers for a three-tier application. Which statement is correct regarding application profiles in Cisco ACI?
a: Application profiles are groups of EPGs.
b: Application profiles are groups of tenants.
c: Application profiles are groups of bridge domains.
d: Application profiles are groups of subnets.

Which Cisco NX-OS Style CLI command triggers a tech support for switch 101, to be stored locally on the controller?
a: techsupport switch 101 local
b: trigger techsupport switch 101
c: start techsupport switch 101
d: start techsupport switch 101 local

