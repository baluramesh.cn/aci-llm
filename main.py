from tasks import task_factory
from datetime import datetime
import guidance
import hashlib
import uuid
import json


def hash_prompt(prompt):
    # Use the utf-8 encoding to convert the string to bytes
    prompt_bytes = prompt.encode('utf-8')

    # Create a new SHA256 hash object
    hasher = hashlib.sha256()

    # Hash the prompt
    hasher.update(prompt_bytes)

    # Get the hexadecimal representation of the hash
    hash_string = hasher.hexdigest()

    return hash_string

def store_result(model, task, prompt, result):
    # root_dir = 'results'
    root_dir = 'experiments'

    # Create the directory for the model if it doesn't exist
    model_dir = os.path.join(root_dir, model)
    os.makedirs(model_dir, exist_ok=True)

    # Create the directory for the task if it doesn't exist
    task_dir = os.path.join(model_dir, task)
    os.makedirs(task_dir, exist_ok=True)

    # Compute the hash of the prompt
    prompt_hash = hash_prompt(prompt)
    # Create the filename for the result file
    result_file = os.path.join(task_dir, f"{prompt_hash}.json")

    # If the file already exists, load the existing results
    if os.path.exists(result_file):
        with open(result_file, 'r') as f:
            existing_results = json.load(f)
    else:
        existing_results = []

    # Append the new result to the existing results
    existing_results.append(result)

    # Write the results back to the file
    with open(result_file, 'w') as f:
        json.dump(existing_results, f, indent=4)


def run_experiment(task_id, model_id, **kwargs):
    guidance.llm = guidance.llms.OpenAI(model_id)
    task = task_factory(task_id, **kwargs)
    assert guidance.llm
    results = task()

    output = {"txn_id": str(uuid.uuid1()), "task": task_id, "model": model_id, "datetime": datetime.now().isoformat()}
    output.update(results)

    # storing logic
    prompt_dict = output['prompt']
    filename = "_".join(prompt_dict.values())
    store_result(model_id, task_id, filename, output)

if __name__ == "__main__":
    import os
    assert("OPENAI_API_KEY" in os.environ)

    # run_experiment(task_id="udemy", model_id="text-davinci-003", prompt_id=0)
    # run_experiment(task_id="udemy", model_id="text-davinci-003", prompt_id=1)
    # run_experiment(task_id="udemy", model_id="text-davinci-003", prompt_id=2)
    # run_experiment(task_id="udemy", model_id="text-davinci-003", prompt_id=3)
    # run_experiment(task_id="udemy", model_id="text-davinci-003", prompt_id=4)
    # run_experiment(task_id="udemy", model_id="text-davinci-003", prompt_id=5)

    # run_experiment(task_id="udemy1", model_id="text-davinci-003", prompt_id=0)
    # run_experiment(task_id="udemy1", model_id="text-davinci-003", prompt_id=1)
    # run_experiment(task_id="udemy1", model_id="text-davinci-003", prompt_id=2)
    # run_experiment(task_id="udemy1", model_id="text-davinci-003", prompt_id=3)
    # run_experiment(task_id="udemy1", model_id="text-davinci-003", prompt_id=4)
    # run_experiment(task_id="udemy1", model_id="text-davinci-003", prompt_id=5)

    # run_experiment(task_id="udemy3", model_id="text-davinci-003", prompt_id=0)
    # run_experiment(task_id="udemy3", model_id="text-davinci-003", prompt_id=1)
    # run_experiment(task_id="udemy3", model_id="text-davinci-003", prompt_id=2)
    # run_experiment(task_id="udemy3", model_id="text-davinci-003", prompt_id=3)
    # run_experiment(task_id="udemy3", model_id="text-davinci-003", prompt_id=4)
    # run_experiment(task_id="udemy3", model_id="text-davinci-003", prompt_id=5)

    # run_experiment(task_id="udemy5", model_id="text-davinci-003", prompt_id=0)
    # run_experiment(task_id="udemy5", model_id="text-davinci-003", prompt_id=1)
    # run_experiment(task_id="udemy5", model_id="text-davinci-003", prompt_id=2)
    # run_experiment(task_id="udemy5", model_id="text-davinci-003", prompt_id=3)
    # run_experiment(task_id="udemy5", model_id="text-davinci-003", prompt_id=4)
    # run_experiment(task_id="udemy5", model_id="text-davinci-003", prompt_id=5)




    # run_experiment(task_id="udemy", model_id="gpt-3.5-turbo-16k", prompt_id=0)
    # run_experiment(task_id="udemy", model_id="gpt-3.5-turbo-16k", prompt_id=1)
    # run_experiment(task_id="udemy", model_id="gpt-3.5-turbo-16k", prompt_id=2)
    # run_experiment(task_id="udemy", model_id="gpt-3.5-turbo-16k", prompt_id=3)
    # run_experiment(task_id="udemy", model_id="gpt-3.5-turbo-16k", prompt_id=4)
    # run_experiment(task_id="udemy", model_id="gpt-3.5-turbo-16k", prompt_id=5)

    # run_experiment(task_id="udemy1", model_id="gpt-3.5-turbo-16k", prompt_id=0)
    # run_experiment(task_id="udemy1", model_id="gpt-3.5-turbo-16k", prompt_id=1)
    # run_experiment(task_id="udemy1", model_id="gpt-3.5-turbo-16k", prompt_id=2)
    # run_experiment(task_id="udemy1", model_id="gpt-3.5-turbo-16k", prompt_id=3)
    # run_experiment(task_id="udemy1", model_id="gpt-3.5-turbo-16k", prompt_id=4)
    # run_experiment(task_id="udemy1", model_id="gpt-3.5-turbo-16k", prompt_id=5)

    # run_experiment(task_id="udemy3", model_id="gpt-3.5-turbo-16k", prompt_id=0)
    # run_experiment(task_id="udemy3", model_id="gpt-3.5-turbo-16k", prompt_id=1)
    # run_experiment(task_id="udemy3", model_id="gpt-3.5-turbo-16k", prompt_id=2)
    # run_experiment(task_id="udemy3", model_id="gpt-3.5-turbo-16k", prompt_id=3)
    # run_experiment(task_id="udemy3", model_id="gpt-3.5-turbo-16k", prompt_id=4)
    # run_experiment(task_id="udemy3", model_id="gpt-3.5-turbo-16k", prompt_id=5)

    # run_experiment(task_id="udemy5", model_id="gpt-3.5-turbo-16k", prompt_id=0)
    # run_experiment(task_id="udemy5", model_id="gpt-3.5-turbo-16k", prompt_id=1)
    # run_experiment(task_id="udemy5", model_id="gpt-3.5-turbo-16k", prompt_id=2)
    # run_experiment(task_id="udemy5", model_id="gpt-3.5-turbo-16k", prompt_id=3)
    # run_experiment(task_id="udemy5", model_id="gpt-3.5-turbo-16k", prompt_id=4)
    # run_experiment(task_id="udemy5", model_id="gpt-3.5-turbo-16k", prompt_id=5)

    run_experiment(task_id="all", model_id="text-davinci-003", prompt_id=0)
    run_experiment(task_id="all", model_id="text-davinci-003", prompt_id=1)
    run_experiment(task_id="all", model_id="text-davinci-003", prompt_id=2)
    run_experiment(task_id="all", model_id="text-davinci-003", prompt_id=3)
    run_experiment(task_id="all", model_id="text-davinci-003", prompt_id=4)
    run_experiment(task_id="all", model_id="text-davinci-003", prompt_id=5)

    run_experiment(task_id="all1", model_id="text-davinci-003", prompt_id=0)
    run_experiment(task_id="all1", model_id="text-davinci-003", prompt_id=1)
    run_experiment(task_id="all1", model_id="text-davinci-003", prompt_id=2)
    run_experiment(task_id="all1", model_id="text-davinci-003", prompt_id=3)
    run_experiment(task_id="all1", model_id="text-davinci-003", prompt_id=4)
    run_experiment(task_id="all1", model_id="text-davinci-003", prompt_id=5)

    run_experiment(task_id="all3", model_id="text-davinci-003", prompt_id=0)
    run_experiment(task_id="all3", model_id="text-davinci-003", prompt_id=1)
    run_experiment(task_id="all3", model_id="text-davinci-003", prompt_id=2)
    run_experiment(task_id="all3", model_id="text-davinci-003", prompt_id=3)
    run_experiment(task_id="all3", model_id="text-davinci-003", prompt_id=4)
    run_experiment(task_id="all3", model_id="text-davinci-003", prompt_id=5)

    run_experiment(task_id="all5", model_id="text-davinci-003", prompt_id=0)
    run_experiment(task_id="all5", model_id="text-davinci-003", prompt_id=1)
    run_experiment(task_id="all5", model_id="text-davinci-003", prompt_id=2)
    run_experiment(task_id="all5", model_id="text-davinci-003", prompt_id=3)
    run_experiment(task_id="all5", model_id="text-davinci-003", prompt_id=4)
    run_experiment(task_id="all5", model_id="text-davinci-003", prompt_id=5)




    run_experiment(task_id="all", model_id="gpt-3.5-turbo-16k", prompt_id=0)
    run_experiment(task_id="all", model_id="gpt-3.5-turbo-16k", prompt_id=1)
    run_experiment(task_id="all", model_id="gpt-3.5-turbo-16k", prompt_id=2)
    run_experiment(task_id="all", model_id="gpt-3.5-turbo-16k", prompt_id=3)
    run_experiment(task_id="all", model_id="gpt-3.5-turbo-16k", prompt_id=4)
    run_experiment(task_id="all", model_id="gpt-3.5-turbo-16k", prompt_id=5)

    run_experiment(task_id="all1", model_id="gpt-3.5-turbo-16k", prompt_id=0)
    run_experiment(task_id="all1", model_id="gpt-3.5-turbo-16k", prompt_id=1)
    run_experiment(task_id="all1", model_id="gpt-3.5-turbo-16k", prompt_id=2)
    run_experiment(task_id="all1", model_id="gpt-3.5-turbo-16k", prompt_id=3)
    run_experiment(task_id="all1", model_id="gpt-3.5-turbo-16k", prompt_id=4)
    run_experiment(task_id="all1", model_id="gpt-3.5-turbo-16k", prompt_id=5)

    run_experiment(task_id="all3", model_id="gpt-3.5-turbo-16k", prompt_id=0)
    run_experiment(task_id="all3", model_id="gpt-3.5-turbo-16k", prompt_id=1)
    run_experiment(task_id="all3", model_id="gpt-3.5-turbo-16k", prompt_id=2)
    run_experiment(task_id="all3", model_id="gpt-3.5-turbo-16k", prompt_id=3)
    run_experiment(task_id="all3", model_id="gpt-3.5-turbo-16k", prompt_id=4)
    run_experiment(task_id="all3", model_id="gpt-3.5-turbo-16k", prompt_id=5)

    run_experiment(task_id="all5", model_id="gpt-3.5-turbo-16k", prompt_id=0)
    run_experiment(task_id="all5", model_id="gpt-3.5-turbo-16k", prompt_id=1)
    run_experiment(task_id="all5", model_id="gpt-3.5-turbo-16k", prompt_id=2)
    run_experiment(task_id="all5", model_id="gpt-3.5-turbo-16k", prompt_id=3)
    run_experiment(task_id="all5", model_id="gpt-3.5-turbo-16k", prompt_id=4)
    run_experiment(task_id="all5", model_id="gpt-3.5-turbo-16k", prompt_id=5)