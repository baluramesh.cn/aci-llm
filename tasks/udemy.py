from .base_task import MCQAFewShot

hf_prompt_dashed = {"header": "", "pre": "Question: ", "mid": "\nChoices:", "post": "Answer: ", "sep": "\n---"}
hf_prompt = {"header": "", "pre": "Question: ", "mid": "\nChoices:", "post": "Answer: ", "sep": "\n"}

mmlu_dashed = {"header": "The following are multiple choice questions (with answers)\n", "pre": "", "mid": "", "post": "Answer: ", "sep": "\n---"}
mmlu = {"header": "The following are multiple choice questions (with answers)\n", "pre": "", "mid": "", "post": "Answer: ", "sep": "\n"}

# TODO: instruct prompt
instruct_dashed = {"header": "The following are multiple choice questions related to Cisco ACI (with answers)\n", "pre": "", "mid": "", "post": "Answer: ", "sep": "\n---"}
instruct = {"header": "The following are multiple choice questions related to Cisco ACI (with answers)\n", "pre": "", "mid": "", "post": "Answer: ", "sep": "\n"}


class UdemyMCQA(MCQAFewShot):
    available_prompts = [hf_prompt_dashed, hf_prompt, mmlu_dashed, mmlu, instruct_dashed, instruct]
    def __init__(self, prompt_id=0):
        prompt = self.available_prompts[prompt_id]
        self.dataset_id = "udemy-mcqa"
        super().__init__(self.dataset_id, prompt, num_shots=0)


class UdemyOneshot(MCQAFewShot):
    available_prompts = [hf_prompt_dashed, hf_prompt, mmlu_dashed, mmlu, instruct_dashed, instruct]
    def __init__(self, prompt_id=0):
        prompt = self.available_prompts[prompt_id]
        self.dataset_id = "udemy-mcqa"
        super().__init__(self.dataset_id, prompt, num_shots=1)


class UdemyThreeshot(MCQAFewShot):
    available_prompts = [hf_prompt_dashed, hf_prompt, mmlu_dashed, mmlu, instruct_dashed, instruct]
    def __init__(self, prompt_id=0):
        prompt = self.available_prompts[prompt_id]
        self.dataset_id = "udemy-mcqa"
        super().__init__(self.dataset_id, prompt, num_shots=3)


class UdemyFiveshot(MCQAFewShot):
    available_prompts = [hf_prompt_dashed, hf_prompt, mmlu_dashed, mmlu, instruct_dashed, instruct]
    def __init__(self, prompt_id=0):
        self.dataset_id = "udemy-mcqa"
        super().__init__(self.dataset_id, self.available_prompts[prompt_id], num_shots=5)

